Projet de proposition pour un nouveau site vitrine pour la [Scic
Tetris](http://scic-tetris.org/).

Pour voir le site en cours de création [c'est par
ici](https://kapu.frama.io/tetris_website/).

TODO:

 * Faire en sorte que lorsque que click sur les articles = animation
type [dimension](https://html5up.net/dimension). Le code de novo, ne pas ré-utiliser dimension, trop compliqué^^.
Utiliser addeventlistener, cf [OCR](https://openclassrooms.com/courses/creez-des-pages-web-interactives-avec-javascript/reagissez-a-des-evenements-1)


 * penser aux crédits
 * code couleur et photo
 * pour contacter: un mailto avec du JS pour camoufler l'adresse mail?
